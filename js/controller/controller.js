export let addTask = () => {
  let newTask = document.getElementById("newTask").value;
  return newTask;
};

export let renderTaskList = (taskList, domID) => {
  let contentHTML = "";
  taskList.forEach((task) => {
    contentHTML += `
        <li>
        <p>${task}</p>
        <p class="buttons">
            <i onclick="deleteTask('${task}')" class="fa fa-trash-alt remove"></i>
            <i onclick="completeTask('${task}')" class="fa fa-check-circle complete"></i>
        </p>
    </li>
        `;
  });
  document.getElementById(domID).innerHTML = contentHTML;
};

export let timKiemViTri = (taskName, taskList) => {
  let index = taskList.indexOf(taskName);
  return index;
};

export let dataTotest = (num, taskList) => {
  for (let n = 0; n < num; n++) {
    taskList.push(`sing song ${String.fromCharCode(65 + n)}`);
  }
};

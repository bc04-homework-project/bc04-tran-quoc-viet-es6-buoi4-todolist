import {
  addTask,
  dataTotest,
  renderTaskList,
  timKiemViTri,
} from "./controller/controller.js";

const taskList_Local = "taskList_Local";
const completeTaskList_Local = "completeTaskList_Local";

var taskList = [];
var completeTaskList = [];
dataTotest(10, taskList); //add sẵn 1 ít data để test lần đầu đỡ phải nhập tay
renderTaskList(taskList, "todo");

var taskListJson = localStorage.getItem(taskList_Local);
var completeTaskListJson = localStorage.getItem(completeTaskList_Local);
if (taskListJson != null) {
  taskList = JSON.parse(taskListJson);

  renderTaskList(taskList, "todo");
}
if (completeTaskListJson != null) {
  completeTaskList = JSON.parse(completeTaskListJson);

  renderTaskList(completeTaskList, "completed");
}

document.getElementById("addItem").addEventListener("click", () => {
  if (addTask() != "") {
    taskList.push(addTask());
    localStorage.setItem(taskList_Local, JSON.stringify(taskList));
    renderTaskList(taskList, "todo");
  }
});

let deleteTask = (taskName) => {
  let index = timKiemViTri(taskName, taskList);
  // console.log("index: ", index);
  if (index != -1) {
    taskList.splice(index, 1);

    localStorage.setItem(taskList_Local, JSON.stringify(taskList));

    renderTaskList(taskList, "todo");
  }

  let i = timKiemViTri(taskName, completeTaskList);
  // console.log('i: ', i);
  if (i != -1) {
    completeTaskList.splice(i, 1);

    localStorage.setItem(
      completeTaskList_Local,
      JSON.stringify(completeTaskList)
    );

    renderTaskList(completeTaskList, "completed");
  }
};

let completeTask = (taskName) => {
  let index = timKiemViTri(taskName, taskList);
  // console.log("index: ", index);
  if (index != -1) {
    completeTaskList.push(String(taskList.splice(index, 1)));

    localStorage.setItem(taskList_Local, JSON.stringify(taskList));
    localStorage.setItem(
      completeTaskList_Local,
      JSON.stringify(completeTaskList)
    );

    renderTaskList(taskList, "todo");
    renderTaskList(completeTaskList, "completed");
  }
};

document.getElementById("two").addEventListener("click", () => {
  taskList.sort();
  completeTaskList.sort();

  renderTaskList(taskList, "todo");
  renderTaskList(completeTaskList, "completed");
});

document.getElementById("three").addEventListener("click", () => {
  taskList.sort();
  taskList.reverse();
  completeTaskList.sort();
  completeTaskList.reverse();

  renderTaskList(taskList, "todo");
  renderTaskList(completeTaskList, "completed");
});

window.deleteTask = deleteTask;
window.completeTask = completeTask;
